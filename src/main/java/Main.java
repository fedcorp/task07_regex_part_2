import com.fedcorp.TXTFileReader;
import com.fedcorp.TaskExecuror;
import com.fedcorp.text_parts.Punctuation;
import com.fedcorp.text_parts.Sentence;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        TXTFileReader fr = new TXTFileReader(
                "C:\\Users\\olegf\\IdeaProjects\\task09_String_RegularExpressions\\" +
                        "src\\main\\java\\com\\fedcorp\\text_resources\\TextFile.txt");
        String text = fr.read();
        TaskExecuror exec = new TaskExecuror();

        //Очищаємо від повторюваних пробілів, табуляцій..
        text = Punctuation.trimTabs(text);

        //Розбиваємо на речення
        List<String> sentenses = Sentence.splitOnSentences(text);

//        //Завдання 1
//        System.out.println(exec.task1(sentenses));

//        //Завдання 2
//        exec.task2(sentenses);

//        //Завдання 3
//        exec.task3(sentenses);

//        Завдання 4
//        exec.task4(sentenses, 5);

//        Завдання 5
//        exec.task5(sentenses);

//        Завдання 6
//        exec.task6(sentenses);

//        Завдання 7
//        exec.task7(sentenses);

//        Завдання 8
//        exec.task8(sentenses);

//        Завдання 9
//        exec.task9(sentenses, 'н');

//        Завдання 10
//        exec.task10(sentenses, new String[] {"программы", "запросы", "тип", "или"});

//        Завдання 11
//        exec.task11(sentenses,'к','я');

//        Завдання 12
        exec.task12(sentenses, 2);

//        Завдання 13
//        exec.task13(sentenses);

//        Завдання 14
//        exec.task14(sentenses);

//        Завдання 15
//        exec.task15(sentenses);

//        Завдання 16
//        exec.task16(sentenses);
    }
}
