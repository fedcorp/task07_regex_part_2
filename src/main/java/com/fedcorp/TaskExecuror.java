package com.fedcorp;

import com.fedcorp.text_parts.Punctuation;
import com.fedcorp.text_parts.Word;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class TaskExecuror {

    //  Task - 1. Знайти найбільшу кількість речень тексту, в яких є однакові слова. Слова з кількістю букв менше 3-х не враховуються
    public int task1(List<String> sentenses){
        int count = 0;
        for (String sentense : sentenses){
            Set<String> uniqueWords = new HashSet<>();
            for(String word : Word.splitOnWords(sentense)){
                if(word.length()>2) {
                    if (!uniqueWords.add(word)) count++;
                }
            }
        }
        return count;
    }

    //  Task - 2. Вивести всі речення заданого тексту у порядку зростання кількості слів у кожному з них.
    public void task2(List<String> sentenses){
       /* Варіант вирішення завдання, якщо потрібно відфільтрувати слова які складаються з 1 літери
        Collections.sort(sentenses, (new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                List<String> list1 = new ArrayList<>();
                List<String> list2 = new ArrayList<>();
                for(String word : Word.splitOnWords(o1)){
                    if(word.length()>1) {
                        list1.add(word);
                    }
                }
                for(String word : Word.splitOnWords(o2)){
                    if(word.length()>1) {
                        list2.add(word);
                    }
                }
                return list1.size()-list2.size();
            }
        }));
        for(String sentence : sentenses) System.out.println(sentence);
        */
        Collections.sort(sentenses, (o1, o2) -> Word.splitOnWords(o1).length - Word.splitOnWords(o2).length);
        for(String sentence : sentenses) System.out.println(sentence);
    }

    //  Task - 3. Знайти таке слово у першому реченні, якого немає ні в одному з інших речень.
    //Виводить усі слова, яких немає в наступних реченнях
    public void task3(List<String> sentenses){
        List<String> distinctList = new ArrayList<>();
        String[] firstSentence = Word.splitOnWords(sentenses.get(0));
        for (String word: firstSentence) {
        boolean isUnique = true;
            for (int i = 1; i<sentenses.size(); i++) {
                String [] words = Word.splitOnWords(sentenses.get(i));
                if(Arrays.stream(words).anyMatch(x->(x.equals(word)))){
                    isUnique=false;
                    continue;
                }
            }
            if(isUnique) distinctList.add(word);
        }
        for (String tmp: distinctList) {
            System.out.println(tmp);
        }
    }

    //  Task - 4. У всіх запитальних реченнях тексту знайти і надрукувати без повторів
    //слова заданої довжини. charLength - кількість символів у слові
    public void task4(List<String> sentenses, int charLength){
        Pattern p = Pattern.compile(".+\\?\\s*");
        Matcher m;
//        for(String tmp : sentenses){
//            m = p.matcher(tmp);
//        }
         Object [] sentences = sentenses.stream()
                .filter(s ->p.matcher(s).find())
                .toArray();
         for (Object sentence : sentences){
             Object [] wordsArr = Arrays.stream(sentence.toString().split("\\s+"))
                     .map(s -> Punctuation.trimPunct(s))
                     .distinct()
                     .filter(s -> s.length()==charLength)
                     .toArray();
             for(Object o:wordsArr) System.out.println(o);
         }

    }

    //  Task - 5. У кожному реченні тексту поміняти місцями перше слово, що починається
    //на голосну букву з найдовшим словом.
    public void task5(List<String> sentenses){
        Pattern p = Pattern.compile("\\b[АаЕєЄєИиІіЇїОоУуЮюЯяAaOoIiUuEe][\\wА-Яа-яїЇіІйЙъЪыЫ\\-_']*\\b");
        List<String> sentensesReplaced = new ArrayList<>();
        for (String s : sentenses){
            String[] words = Word.splitOnWords(s);
            String longestWord = "";
            String firstVovelLetterWord = "";
            Matcher m = p.matcher(s);
            if(m.find()){
                firstVovelLetterWord = m.group();
            }
            int count = 0;
            for(String word : words){
                if (word.length()>count){
                    longestWord = word;
                    count = word.length();
                }
            }
            if (!longestWord.isEmpty() && !firstVovelLetterWord.isEmpty() && !longestWord.equals(firstVovelLetterWord)){
                System.out.println(longestWord);
                System.out.println(firstVovelLetterWord);
                if(s.indexOf(firstVovelLetterWord)<s.indexOf(longestWord)) {
                    String s2 = s.replaceFirst("\\b"+longestWord+"\\b", firstVovelLetterWord);
                    sentensesReplaced.add(s2.replaceFirst("\\b"+firstVovelLetterWord+"\\b", longestWord));
                }else{
                    String s2 = s.replaceFirst("\\b"+firstVovelLetterWord+"\\b", longestWord);
                    sentensesReplaced.add(s2.replaceFirst("\\b"+longestWord+"\\b", firstVovelLetterWord));
                }
            }
        }
        for (String s : sentensesReplaced){
            System.out.println(s);
        }
    }

    //  Task - 6. Надрукувати слова тексту в алфавітному порядку по першій букві. Слова,
    //що починаються з нової букви, друкувати з абзацного відступу.
    public void task6(List<String> sentenses){
        List<String> list = new ArrayList<>();
        for (String s : sentenses){
            String [] words = Word.splitOnWords(s);
            for(String word : words){
                if (word.length()!=0)
                list.add(word);
            }
        }
        Collections.sort(list);
        String tmp = "";
        for (String word : list){
            if(tmp.length()!=0 && tmp.charAt(0)==word.charAt(0)) {
                System.out.println(word);
            }else{
                System.out.println();
                System.out.println(word);
            }
            tmp = word;
        }
    }

    //  Task - 7. Відсортувати слова тексту за зростанням відсотку голосних букв
    //(співвідношення кількості голосних до загальної кількості букв у слові).
    public void task7(List<String> sentenses) {

        List<String> list = new ArrayList<>();

        for (String s : sentenses){
            String [] words = Word.splitOnWords(s);
            for(String word : words){
                //Фільтруємо слова які складаються з одної літери
                if (word.length()>1)
                    list.add(word);
            }
        }

        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Double word1;
                Double word2;
                int wordLength1;
                int wordLength2;
                int vowelsCount1=0;
                int vowelsCount2=0;
                wordLength1 =o1.length();
                wordLength2 =o2.length();
                Pattern p = Pattern.compile("[АаЕеЄєИиІіЇїОоУуЮюЯяaAoOiIeEuU]");
                Matcher m = p.matcher(o1);
                while(m.find()){
                    vowelsCount1++;
                }
                word1 = (double)vowelsCount1 / (double)wordLength1;
                Matcher m2 = p.matcher(o2);
                while(m2.find()){
                    vowelsCount2++;
                }
                System.out.println();
                System.out.println(vowelsCount1);
                System.out.println(wordLength1);
                System.out.println(vowelsCount2);
                System.out.println(wordLength2);
                word2 = (double)vowelsCount2 / (double)wordLength2;
                //Якщо відсоток голосних у слові одинаковий - сортуємо в алфавітному порядку для зручності читання
                if(word1.equals(word2)) return o1.compareTo(o2);
                return Double.compare(word1,word2);
            }
        });

        for (String word : list){
                System.out.println(word);
        }
    }

    //  Task - 8. Слова тексту, що починаються з голосних букв, відсортувати в
    //алфавітному порядку по першій приголосній букві слова.
    public void task8(List<String> sentenses) {
        Pattern p = Pattern.compile("\\b[АаЕеЄєИиІіЇїОоУуЮюЯяaAoOiIeEuU]");
        List<String> list = new ArrayList<>();

        for (String s : sentenses){
            String [] words = Word.splitOnWords(s);
            for(String word : words) {
                Matcher m = p.matcher(word);
                if (m.find()) {
                    //Фільтруємо слова які складаються з одної літери
                    if (word.length() > 1)
                        list.add(word);
                }
            }
        }

        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Pattern p =Pattern.compile("[B-Zb-zБ-Яб-я&&[^АаЕеЄєИиІіЇїОоУуЮюЯяaAoOiIeEuU]]");
                Matcher m = p.matcher(o1);
                String tmp1 = "";
                String tmp2 = "";
                if(m.find()){
                    tmp1 = o1.substring(m.start());
                }
                m = p.matcher(o2);
                if(m.find()){
                    tmp2 = o2.substring(m.start());
                }
                if(tmp1.length()!=0||tmp2.length()!=0) {
                    return tmp1.compareTo(tmp2);
                }
                return 0;
            }
        });

        for (String word : list){
            System.out.println(word);
        }
    }

    //  Task - 9. Всі слова тексту відсортувати за зростанням кількості заданої букви у
    //слові. Слова з однаковою кількістю розмістити у алфавітному порядку.
    public void task9(List<String> sentenses, char value) {
        List<String> list = new ArrayList<>();
        Pattern p = Pattern.compile(String.valueOf(value).toLowerCase());
        for (String s : sentenses){
            String [] words = Word.splitOnWords(s);
            for(String word : words) {
                list.add(word.toLowerCase());
            }
        }
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Matcher m;
                int counterWord1=0;
                int counterWord2=0;
                m = p.matcher(o1);
                while(m.find()) {
                    counterWord1++;
                }
                m = p.matcher(o2);
                while(m.find()) {
                    counterWord2++;
                }
                if (counterWord1==counterWord2) return o1.compareTo(o2);
                return counterWord1-counterWord2;
            }
        });

        for (String word : list){
            System.out.println(word);
        }
    }

    //  Task - 10. Є текст і список слів. Для кожного слова з заданого списку знайти, скільки
    //разів воно зустрічається у кожному реченні, і відсортувати слова за
    //спаданням загальної кількості входжень.
    public void task10(List<String> sentenses, String[] words) {
        //Потрібно відсортувати мап за значенням!

        HashMap<String, Integer> wordsCounter = new HashMap<>();
        for(String word : words){
            if(!wordsCounter.containsKey(word.trim().toLowerCase()))
                wordsCounter.put(word.trim().toLowerCase(), 0);
        }
        for (String s : sentenses){
            String [] wordsInSentence = Word.splitOnWords(s);
            for(String word : wordsInSentence) {
                if(wordsCounter.containsKey(word)){
                    wordsCounter.put(word, wordsCounter.get(word)+1);
                }
            }
        }
        List<Map.Entry<String, Integer>> listEntry = new ArrayList<>(wordsCounter.entrySet());
        Collections.sort(listEntry, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));

        for (Map.Entry<String, Integer> tmp : listEntry){
            System.out.print("Слово - ");
            System.out.print("\""+tmp.getKey()+"\"");
            System.out.print(" міститься в тексті ");
            System.out.print(tmp.getValue());
            System.out.println(" раз(и).");
        }
    }

    //  Task - 11. У кожному реченні тексту видалити підрядок максимальної довжини, що
    //починається і закінчується заданими символами.
    public void task11(List<String> sentenses, char a, char b) {

        ArrayList<String> list = new ArrayList<>();
        Pattern p1 = Pattern.compile(String.valueOf(a).toLowerCase());
        Pattern p2 = Pattern.compile(String.valueOf(b).toLowerCase());
        Matcher m;

        for (String s : sentenses) {
            int posFirst = 0;
            int posLast = 0;
            boolean startIsPresent = false;
            boolean endsIsPresent = false;
            StringBuilder someString = new StringBuilder(s);
            m = p1.matcher(s.toLowerCase().trim());
            if (m.find()) {
                posFirst = m.start();
                startIsPresent = true;
            }
            m = p2.matcher(s.toLowerCase().trim());
            while (m.find()) {
                if (m.start() != 0) {
                    posLast = m.start();
                    endsIsPresent = true;
                }
            }
            if (startIsPresent && endsIsPresent && posFirst < posLast) {
                list.add(someString.delete(posFirst, posLast+1).toString());
            }
        }
        for (String s: list) System.out.println(s);
    }

    //  Task - 12. З тексту видалити всі слова заданої довжини, що починаються на
    //приголосну букву. ()  Не заверщене!!! 
    public void task12(List<String> sentenses, int length) {
        List<String> list = new ArrayList<>();
//        Pattern p = Pattern.compile("\\b[а-яА-ЯіІїЇ&&[^АаЕеОоУуИиІіЇї]]");
        Matcher m;

        for (String s : sentenses){
            String s2 = s;
            list.add(s2.replaceAll("\\b[а-яА-ЯіІїЇ&&[^АаЕеОоУуИиІіЇї]][A-Za-zА-Яа-яіІїЇ']{"+(length-1)+"}\\p{Punct}|\\b",""));
//
//            String [] words = Word.splitOnWords(s);
//            for(String word : words) {
//                m = p.matcher(word);
//                if(length == word.length() && m.find()) {
//                    s2 = s2.replaceAll("^|\\b" + word + "\\b|\\p{Punct}", "");
//                }
//            }
//            list.add(s2);
        }
        for (String s : list) System.out.print(s);
    }

    //  Task - 13. Відсортувати слова у тексті за спаданням кількості входжень заданого
    //символу, а у випадку рівності – за алфавітом.
    public void task13(List<String> sentenses, char value) {
        List<String> list = new ArrayList<>();
        Pattern p = Pattern.compile(String.valueOf(value).toLowerCase());
        for (String s : sentenses){
            String [] words = Word.splitOnWords(s);
            for(String word : words) {
                list.add(word.toLowerCase());
            }
        }
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Matcher m;
                int counterWord1=0;
                int counterWord2=0;
                m = p.matcher(o1);
                while(m.find()) {
                    counterWord1++;
                }
                m = p.matcher(o2);
                while(m.find()) {
                    counterWord2++;
                }
                if (counterWord1==counterWord2) return o1.compareTo(o2);
                return counterWord2-counterWord1;
            }
        });

        for (String word : list){
            System.out.println(word);
        }
    }
    public void task14(List<String> sentenses) {}
    public void task15(List<String> sentenses) {
    }
    public void task16(List<String> sentenses) {
    }

}
