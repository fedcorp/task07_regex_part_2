package com.fedcorp.text_parts;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Punctuation {


    public static String trimTabs(String s){
        s = s.replaceAll("([ \\t\\x0B\\f]+)"," ");
        return s;
    }

    public static String trimPunct(String s){
        Pattern p = Pattern.compile("^[\\p{Punct}«]+|[\\p{Punct}»]+$");
        Matcher m = p.matcher(s);
        return m.replaceAll("");
    }
}
