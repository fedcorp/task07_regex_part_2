package com.fedcorp.text_parts;

import java.util.List;

public class Word {

    public static String[] splitOnWords(String text){
        text = text.toLowerCase();
        String[] words = text.split("\\s+");
        for (int i=0; i<words.length; i++) {
            words[i] = Punctuation.trimPunct(words[i]);
        }
        return words;
    }
}
