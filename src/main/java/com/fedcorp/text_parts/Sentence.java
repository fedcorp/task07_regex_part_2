package com.fedcorp.text_parts;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {

    private static Pattern p;
    private static Matcher m;

    public static List<String> splitOnSentences(String string) {
        List<String> sentences = new LinkedList<>();
        p = Pattern.compile("[^\\.\\!\\?]+[!?.]\\s+");
        m = p.matcher(string);
        while (m.find()) {
            sentences.add(string.substring(m.start(), m.end()));

        }
        return sentences;
    }
}
