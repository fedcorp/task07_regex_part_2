package com.fedcorp;

import java.io.*;

public class TXTFileReader {

    private String path;

    public TXTFileReader(String path) {
        this.path = path;
    }

    public String read() {
        String line;
        File filePath = new File(path);
        StringBuilder text = new StringBuilder();
        try (BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"))) {
            while ((line = bf.readLine()) != null) {
                text.append(line + '\n');
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }
}
